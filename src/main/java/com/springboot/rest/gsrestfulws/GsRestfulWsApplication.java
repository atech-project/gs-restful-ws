package com.springboot.rest.gsrestfulws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GsRestfulWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GsRestfulWsApplication.class, args);
	}

}

