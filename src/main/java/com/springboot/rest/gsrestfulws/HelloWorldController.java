package com.springboot.rest.gsrestfulws;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@GetMapping(path="/hello-world")
	public String helloworld() {
		return "hellow world";
	}
	
	@GetMapping(path="/hello-world-bean")
	public HellowWorldBean helloworldBean() {
		return new HellowWorldBean("Hellow World Bean");
	}
	
	@GetMapping(path="/hello-world-bean/{name}")
	public HellowWorldBean helloworldBeanPathVeriable(@PathVariable String name) {
		return new HellowWorldBean(String.format("Hellow World Bean - %s",name));
	}
}
