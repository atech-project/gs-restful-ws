package com.springboot.rest.gsrestfulws.controll;

import java.net.URI;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.springboot.rest.gsrestfulws.dao.GSRestfulWSDAO;
import com.springboot.rest.gsrestfulws.exception.UserNotFoundException;
import com.springboot.rest.gsrestfulws.model.User;

@RestController
public class UserController {

	@Autowired
	private GSRestfulWSDAO gsDAOService;
	
	@GetMapping(path="/users")
	public ArrayList<User> getAllUsers(){
		return gsDAOService.findAll(); 
	}
	
	@GetMapping(path="/users/{id}")
	public User getUserById(@PathVariable Integer id){
		User user = gsDAOService.findById(id);
		if(user == null ) {
			throw new UserNotFoundException("id - "+ id);
		}
		return user; 
	}
	
	@PostMapping(path="/users")
	public ResponseEntity<Object> saveUser(@Validated @RequestBody User user) {
		User userObj = gsDAOService.save(user);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(userObj.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
	
}
