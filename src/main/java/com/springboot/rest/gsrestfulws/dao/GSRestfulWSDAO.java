package com.springboot.rest.gsrestfulws.dao;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.springboot.rest.gsrestfulws.model.User;

@Component
public class GSRestfulWSDAO {

	public static ArrayList<User> arrayList = new ArrayList<User>();
	
	static {
		arrayList.add(new User(1, "Anand", new Date()));
		arrayList.add(new User(2, "Girish", new Date()));
		arrayList.add(new User(3, "Bandu", new Date()));
	}
	
	public ArrayList<User> findAll(){
		return arrayList;
	}

	public User save(User user) {
		arrayList.add(user);
		return user;
	}

	public User findById(int id) {
		for(User user : arrayList) {
			if(user.getId() == id) {
				return user;
			}
		}
		return null;
	}
	
}
